<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
 <%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
 <h:import url="nichos/header.jsp"><h:param name="titulo" value="Principal"></h:param></h:import>
 <body>
	<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${sessionScope.user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=atualizarTime'>Editar Time</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=verFunci&time=${user}'>Funcionários</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${param.email}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=atualizarTime'>Editar Time</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=verFunci&time=${user}'>Funcionários</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=atualizarTime'>Editar Time</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=verFunci&time=${user}'>Funcionários</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<br />
		<div class='ui container'>
			<center>
				<h1 style='color: #080b34;'>
					Partidas
				</h1>
			</center>
			<table class="ui blue table">
  				<thead>
    				<tr>
    					<th>Adiversário</th>
    					<th>Data</th>
    					<th>Hora</th>
    					<th>Visualizar</th>
    					<th>Alterar</th>
    					<th>Deletar</th>
  					</tr>
  				</thead>
  				<tbody>
    				<h:set var="lista" value="${requestScope['lista']}"></h:set>
			<h:forEach items="${lista}" var="partida">
				<tr>
					<td><h:out value="${partida.adv}"></h:out></td>
      				<td><h:out value="${partida.data}"></h:out></td>
      				<td><h:out value="${partida.hora}"></h:out></td>
      				<td><a href="${pageContex.request.contexPath}home?pagina=visua&time=${user}"><i class="file alternate outline icon"></i></a></td>
      				<td><a href="${pageContex.request.contexPath}home?pagina=edite&time=${partida.id}"><i class="edit icon"></i></a></td>
      				<td><a href="${pageContex.request.contexPath}home?pagina=rem&time=${partida.id}"><i class="close icon"></i></a></td>
    			</tr>
			</h:forEach>
  				</tbody>
			</table>
		</div>
	</div> <!-- fim menu pusher -->
	
</body>
<h:import url="nichos/scripts.jsp"></h:import>

</html>