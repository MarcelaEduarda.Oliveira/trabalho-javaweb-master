<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Loguin"></h:param></h:import>
	<body>
		<h:import url="nichos/menu.jsp"></h:import>
		<!-- Formulario Cadastro -->
		<div style='background-color: #080b34;'>
		<div class='ui vertical stripe segment'>
		    <div class='ui middle aligned stackable grid container'>
		      <div class='row'>
		        <div class='eight wide column'>
		          <img src='Visual/imagens/imgtw.png' >
		        </div>
		        <div class='six wide right floated column'>
		        <div class='ui middle aligned center aligned grid'>
				<div class='column' style='margin-top: -100px;'>

		        <h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
					Entre em sua conta
				</h1>
		          <form class='ui large form' action='' method='post'>
					<div class='ui stacked segment'>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='envelope icon'></i>
								<input type='text' name='email' placeholder='Email'>
							</div>
						</div>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='lock icon'></i>
								<input type='password' name='senha' placeholder='Senha' id='senha'>
							</div>
						</div>
						<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Cadastrar' />
					</div>

					<div class='ui error message'></div>

				</form>
				<div class='ui message'>
					Não tem conta? <a href='Cadastro.jsp'> Clique aqui</a>
				</div>
				</div>
		      </div>
		        </div>
		      </div>
		    </div>
		  </div>
		 </div>
	</div>
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>