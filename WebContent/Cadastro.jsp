<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>

<h:import url="nichos/header.jsp"><h:param name="titulo" value="Cadastro"></h:param></h:import>
<body >
		<h:import url="nichos/menu.jsp"></h:import>
		<!-- Formulario Cadastro -->
		<div style='background-color: #080b34;'>
		<div class='ui vertical stripe segment'>
		    <div class='ui middle aligned stackable grid container'>
		      <div class='row'>
		        <div class='eight wide column'>
		          <img src='Visual/imagens/imgtw.png' >
		        </div>
		        <div class='six wide right floated column'>
		        <div class='ui middle aligned center aligned grid'>
				<div class='column' style='margin-top: -100px;'>

		        <h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
					Crie sua conta
				</h1>
		          <form class='ui large form' action='home' method='post'>
		          <input type='hidden' name='form' value='cadastro'>
					<div class='ui stacked segment'>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='user icon'></i>
								<input type='text' name='name' placeholder='Nome do Time'>
							</div>
						</div>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='envelope icon'></i>
								<input type='email' name='email' placeholder='Email do Time'>
							</div>
						</div>
						<h:if test="${mensagem.existeErros}">
						    <div id="erro">
						        <h:forEach var="erro" items="${mensagem.erros}">
						             <h5 style='color: red;'> ${erro} </h5>
						        </h:forEach>
						    </div>
						</h:if>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='lock icon'></i>
								<input type='password' name='senha' placeholder='Senha' id='senha'>
							</div>
						</div>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='lock icon'></i>
								<input type='password' name='confsenha' placeholder='Confirmar senha' id='confsenha'>
							</div>
						</div>
						<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Cadastrar' />
					</div>
				</form>
				<div class='ui message'>
					Já tem uma conta? <a href='${pageContex.request.contexPath}home?pagina=login'> Clique aqui</a>
				</div>
				</div>
		      </div>
		        </div>
		      </div>
		    </div>
		  </div>
		 </div>
	</div> <!-- Fim menu pusher -->
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>