<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<% 
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Cadastro Partida"></h:param></h:import>
<body>
	<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${user}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<div style='background-color: #080b34;'>
		<br /><br /><br>
			<center>
				<h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
					Cadastro da Partida
				</h1>
			</center>
			<br><br />
			<div class='ui middle  center aligned grid'>
			   <form class='ui  form' action='home' method='post'>
			   <input type='hidden' name='form' value='partida'>
			   <input type='hidden' name='email' value='${user}'>
				  <div class='ui stacked segment'>
					<div class='ui form'>
					  <div class='two fields'>
					    <div class='field'>
					      <label>Local</label>
					      <input placeholder='' name='local' type='text' required>
					    </div>
					    <div class='field'>
					      <label>Juiz</label>
					      <input name='juiz' type='text' required>
					    </div>
					  </div>
					</div>
					<div class='ui form'>
					  <div class='two fields'>
					    <div class='field'>
					      <label>Time Adversario</label>
					      <input name='adv' type='text' required>
					    </div>
					    <div class='field'>
					      <label>Vencedor</label>
					      <input  name='vencedor' type='text' required>
					    </div>
					 </div>
				   </div>
				   <div class='ui form'>
				    <div class='two fields'>
				      <div class='field'>
					      <label>Data</label>
					      <input  name='data' type='date'  required>
				      </div>
				      <div class='field'>
					      <label>Hora</label>
					      <input  name='hora' type='time'  required>
				      </div>
				   </div>
				  </div>
				  <input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Cadastrar' />
			</div>
		</form>
	</div>		
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />	
	</div>		
</div>
</body>
	<h:import url="nichos/scripts.jsp"></h:import>
</html>