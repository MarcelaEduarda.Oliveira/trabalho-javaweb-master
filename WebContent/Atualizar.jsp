<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
 <%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Login"></h:param></h:import>
	<body>
		<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${sessionScope.user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${param.email}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a  class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=editef&time=${user}'>Editar Funcionarios</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Formulario Cadastro -->
		<div style='background-color: #080b34;'>
		<div class='ui vertical stripe segment'>
		    <div class='ui middle aligned stackable grid container'>
		      <div class='row'>
		        <div class='eight wide column'>
		          <img src='Visual/imagens/imgtw.png' >
		        </div>
		        <div class='six wide right floated column'>
		        <div class='ui middle aligned center aligned grid'>
				<div class='column' style='margin-top: -100px;'>

		        <h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
					Atualize sua conta
				</h1>
		          <form class='ui large form' action='' method='post'>
		          <input type='hidden' name='form' value='atualizar'>
					<div class='ui stacked segment'>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='envelope icon'></i>
								<input type='text' id='email' name='email' placeholder='Seu Email'>
							</div>
						</div>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='lock icon'></i>
								<input type='password' name='senha' placeholder='Nova Senha' id='email'>
							</div>
						</div>
            			<h:if test='${mensagens.existeErros}'>
            				<div id='erro'>
 
                    				<h:forEach var='erro' items='${mensagens.erros}'>
                        				<h5 style='color: red;'> ${erro} </h5>
                        			</h:forEach>
            				</div>
        				</h:if>
						<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Atualizar' />
					</div>
				</form>
				</div>
		      </div>
		        </div>
		      </div>
		    </div>
		  </div>
		 </div>
	</div>
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>