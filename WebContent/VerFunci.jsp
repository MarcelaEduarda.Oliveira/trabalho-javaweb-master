<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Atividade de Lab. Soft."></h:param></h:import>
<body>
		<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${user}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<h:choose>
            <h:when test="${not empty requestScope['funcioEdit']}">
                 <h:set var="funcio" value="${requestScope['funcioEdit']}"></h:set>
            </h:when>
             <h:when test="${requestScope['funcioEdit'] == null}">
                 	<h:redirect url="home?pagina=principal"></h:redirect>
              </h:when>
        </h:choose>
		 <div class="ui vertical stripe segment">
    		<div class="ui text container">
      			<h3 class="ui header">Funcion�rios</h3>
      				<table class="ui blue table">
  				<thead>
    				<tr>
    					<th>Nome</th>
    					<th>Email</th>
    					<th>Fun��o</th>
  					</tr>
  				</thead>
  				<tbody>
				<tr>
					<td><h:out value="${funcio.nomet}"></h:out></td>
      				<td><h:out value="${funcio.emailt}"></h:out></td>
      				<td>T�cnico</td>
    			</tr>
    			<tr>
					<td><h:out value="${funcio.nomea}"></h:out></td>
      				<td><h:out value="${funcio.emaila}"></h:out></td>
      				<td>Auxiliar T�cnico</td>
    			</tr>
  				</tbody>
			</table>
      		</div>
      	</div>
</div>
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>