<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Atividade de Lab. Soft."></h:param></h:import>
<body>
		<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${user}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>

		 <div class="ui vertical stripe segment">
    		<div class="ui text container">
				<div class='ui container'>
			<center>
				<h1 style='color: #080b34;'>
					Pontua��o dos Jogadores
				</h1>
			</center>
			<table class="ui blue table">
  				<thead>
    				<tr>
    					<th>Jogador</th>
    					<th>Camisa </th>
    					<th>Pontua��o</th>
  					</tr>
  				</thead>
  				<tbody>
				<h:choose>
                 	<h:when test="${not empty requestScope['joga']}">
                 		<h:set var="funcio" value="${requestScope['joga']}"></h:set>
                 	</h:when>
                 	<h:when test="${requestScope['joga'] == null}">
                 		<h:redirect url="home?pagina=principal"></h:redirect>
                 	</h:when>
                 </h:choose>
				<tr>
					<td><h:out value="${joga.nome1}"></h:out></td>
      				<td><h:out value="${joga.camisa1}"></h:out></td>
      				<td><h:out value="${joga.pontos1}"></h:out></td>
    			</tr>
				<tr>
					<td><h:out value="${joga.nome2}"></h:out></td>
      				<td><h:out value="${joga.camisa2}"></h:out></td>
      				<td><h:out value="${joga.pontos2}"></h:out></td>
    			</tr>
  				</tbody>
			</table>
		</div>
      		</div>
      	</div>
</div>
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>