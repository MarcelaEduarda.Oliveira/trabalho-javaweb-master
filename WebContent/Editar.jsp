<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
 <h:import url="nichos/header.jsp"><h:param name="titulo" value="Editar"></h:param></h:import>
<body>
	<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${sessionScope.user}</a>
				<div class='right menu'>
					<a  class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${param.email}</a>
			<a  class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a  class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<div class='ui container'>
    		<div class='ui vertical stripe segment'>
    		<center>
		     <h2 class="ui teal image header">
      			<img src="Visual/imagens/icon.png" class="image"/>
      				<div class="content" style='color: #080b34;'>
                        Editar Informações
      				</div>
    		</h2>
			</center>
			<br /><br />
		    <div class='ui middle aligned stackable grid container'>
		      <div class='row'>
		      <div class='eight wide column'>
				<h:choose>
                 	<h:when test="${not empty requestScope['partidaEdit']}">
                 		<h:set var="partida" value="${requestScope['partidaEdit']}"></h:set>
                 	</h:when>
                 	<h:when test="${requestScope['partidaEdit'] == null}">
                 		<h:redirect url="home?pagina=principal"></h:redirect>
                 	</h:when>
                 </h:choose>
                 <form class='ui  form' action='home' method='post'>
			   		<input type='hidden' name='form' value='editarpartida'>
			   		<input type='hidden' name='email' value='${user}'>
			   		<input type='hidden' name='id' value='${partida.id}'>
				  	<div class='ui stacked segment'>
						<div class='ui form'>
					  		<div class='two fields'>
					    		<div class='field'>
					      			<label>Local</label>
					      			<input  name='local' type='text' value='${partida.local}' required>
					    		</div>
					   			 <div class='field'>
					      			<label>Juiz</label>
					     	 		<input  name='juiz' type='text' value='${partida.juiz}' required>
					    		</div>
					  		</div>
						</div>
						<div class='ui form'>
					  		<div class='two fields'>
					    		<div class='field'>
					     			<label>Time Adversário</label>
					      			<input  name='adv' type='text' value='${partida.adv}' required>
					    		</div>
					    		<div class='field'>
					      			<label>Time Vencedor</label>
					      			<input  name='vencedor' type='text' value='${partida.vencedor}' required>
					    		</div>
					 		</div>
				   		</div>
				   		<div class='ui form'>
				    		<div class='two fields'>
				      			<div class='field'>
					      			<label>Data</label>
					      			<input  name='data' type='date' value='${partida.data}' required>
				      			</div>
				      			<div class='field'>
					     	 		<label>Hora</label>
					      			<input  name='hora' type='time' value='${partida.hora}'required>
				      			</div>
				   			</div>
				  		</div>
				  		<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Editar Partida' />
					</div>
				</form>
                  
              </div>
                 <div class='six wide right floated column'>
		        <div class='ui middle aligned center aligned grid'>
		        <div class='column'>
		        	 <form class='ui  form' action='home' method='post' style='width: 400px;'>
			   			<input type='hidden' name='form' value='jogadoresEdit'>
			   			<input type='hidden' name='email' value='${user}'>
				  		<div class='ui stacked segment'>
						<div class='ui form'>
						<h:choose>
                 			<h:when test="${not empty requestScope['jogaEdit']}">
                 				<h:set var="joga" value="${requestScope['jogaEdit']}"></h:set>
                 			</h:when>
                 			<h:when test="${requestScope['jogaEdit'] == null}">
                 				<h:redirect url="home?pagina=principal"></h:redirect>
                 			</h:when>
                 		</h:choose>
                 		<input type='hidden' name='id' value='${joga.id}'>
					  		<div class='two fields'>
					    		<div class='field'>
					      			<label>Jogador 1</label>
					      			<input placeholder='' name='nome1' type='text' value='${joga.nome1}' required>
					    		</div>
					    		<div class='field'>
					      			<label>Camisa</label>
					      			<input placeholder='' name='camisa1' type='number' value='${joga.camisa1}' required>
					    		</div>
					  		</div>
						</div>
						<div class='ui form'>
					  		<div class='two fields'>
					    		<div class='field'>
					      			<label>Jogador 2</label>
					      			<input placeholder='' name='nome2' type='text' value='${joga.nome2}' required>
					    		</div>
					    		<div class='field'>
					      			<label>Camisa</label>
					      			<input placeholder='' name='camisa2' type='number' value='${joga.camisa2}' required>
					    		</div>
					 		</div>
				   		</div>
				   		<div class='ui form'>
				    		<div class='two fields'>
				      			<div class='field'>
					      			<label>Pontos Jogador 1</label>
					      			<input  name='pontos1' type='number' value='${joga.pontos1}' required>
				      			</div>
				      			<div class='field'>
					      			<label>Pontos Jogador 2</label>
					      			<input  name='pontos2' type='number'value='${joga.pontos2}' required>
				      			</div>
				   			</div>
				  		</div>
				  		<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Editar informações dos Jogadores' />
					</div>
				</form>
		        </div>
		        </div>
		        </div>
             </div>
             </div> 
           </div>
    </div>         
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>