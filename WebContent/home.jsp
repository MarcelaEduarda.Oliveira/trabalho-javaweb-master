
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>

<h:import url="nichos/header.jsp"><h:param name="titulo" value="Home"></h:param></h:import>
	<body>
		<h:import url="nichos/menu.jsp"></h:import>
		<header>
			<img class='ui fluid image' src='Visual/imagens/banner.png'>
		</header>
		
		<div class='ui vertical stripe segment'>
    		<div class='ui text container'>
     			<h3 class='ui header'>Para que serve o Tênis Web?</h3>
      			<p>Se você quer deixar armazenado de maneira segura, e ter um feadback sobre o jogo do  time que você lidera, esse é o sistema perfeito para você!Ele te ajuda a deixar as informações da partida organizada e faz um relátorio detralhado para você ter controle do jogos!</p>
      		 </div>	
  		</div>
  		<div class='ui vertical stripe segment'>
    		<div class='ui middle aligned stackable grid container'>
      			<div class='row'>
        			<div class='eight wide column'>
          				<h3 class='ui header'>Como funciona ?</h3>
          				<p>É bem intuitivo e rápido!Primeiro você vai ter que Cadastrar o Time, depois os funcionários e informar qual a sua função(Apoio, técnico ou auxiliar) de acordo com sua função vai ser dirigido ao cadastro do time, informações do jogo e afins, no final, um relátorio vai ser gerado e o Técnico e o auxiliar vão poder visualizar de maneira geral tudo sobre a partida!</p>
          				<h3 class='ui header'>Qual a finalidade do relátorio?</h3>
          				<p>Ajuda ao Técnico e o auxiliar a ter uma visão mais ampla e detalhada sobre a partida, o Técnico poderá alterar ou remover as informações caso ache necessário.</p>
        			</div>
       	 			<div class='six wide right floated column'>
          				<img src='Visual/imagens/rel.gif' >
       	 			</div>
      			</div>
      			<div class='row'>
        			<div class='center aligned column'>
          				<a class='ui huge button' href='Visual/Cadastro.jsp' style='background-color: #080b34; color: #FFFFFF;'>Cadastrar</a>
        			</div>
      			</div>
    		</div>
  		</div>
  	  	<div class='ui vertical stripe quote segment'>
			<div class='ui equal width stackable internally celled grid'>
				<div class='center aligned row'>
					<div class='column'>
						<h3>Auxiliar</h3>
						<p>
							Responsável por cadastrar a partida, os jogadores e a pontuação, bem como ver o Relatório final do jogo.
						</p>
					</div>
					<div class='column'>
						<h3>Técnico</h3>
						<p>
							Poderá ver o relátorio, remover informações e alterar caso ache necessário, ele pode se organizar melhor dessa forma.
						</p>
					</div>
				</div>
			</div>
		</div>
			<br /><br />
  </div> <!-- fim menu pusher -->

	<h:import url="nichos/footer.jsp"></h:import>
  		
	</body>
	<h:import url="nichos/scripts.jsp"></h:import>
</html>