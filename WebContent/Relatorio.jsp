<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
<%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Atividade de Lab. Soft."></h:param></h:import>
<body>
	<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${user}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=principal&time=${user}'>Principal</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
		<h:set var="lista" value="${requestScope['lista']}"></h:set>
		 <div class="ui vertical stripe segment">
    		<div class="ui text container">
    		<h:forEach items="${lista}" var="partida">
      			<h3 class="ui header">Hist�rico da Partida do dia <h:out value="${partida.data}"></h:out> </h3>
      				<table class="ui blue table">
  				<thead>
    				<tr>
    					<th>Local</th>
    					<th>Hora</th>
    					<th>Advers�rio</th>
    					<th>Juiz</th>
    					<th>Vencedor</th>
  					</tr>
  				</thead>
  				<tbody>
				<tr>
					<td><h:out value="${partida.local}"></h:out></td>
      				<td><h:out value="${partida.hora}"></h:out></td>
      				<td><h:out value="${partida.adv}"></h:out></td>
      				<td><h:out value="${partida.juiz}"></h:out></td>
      				<td><h:out value="${partida.vencedor}"></h:out></td>
    			</tr>
  				</tbody>
			</table>
      				<a class="ui large button" href="${pageContex.request.contexPath}home?pagina=jogaVisu&time=${partida.id}" style='background-color: #080b34;color: white;'>Jogadores/Pontua��o</a>
      		</h:forEach>
      		</div>
      	</div>
</div>
</body>
<h:import url="nichos/scripts.jsp"></h:import>
</html>