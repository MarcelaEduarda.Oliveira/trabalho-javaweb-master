<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
 <%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
<h:import url="nichos/header.jsp"><h:param name="titulo" value="Principal"></h:param></h:import>
<body>
<br /><br /><br /><br /><br /><br />
	<center>
	<div class='ui container'>
		<div class="ui negative message" style="width: 400px;">
  			<i class="close icon"></i>
 			 <div class="header">
    			Confirmar Remoção
  			</div>
 			<p>Tem certeza que deseja remover partida?</p>
 			<form class='ui  form' action='home' method='post'>
			   <input type='hidden' name='form' value='voltar'>
			   <input type='hidden' name='email' value='${user}'>
    		   		<i class="checkmark icon" style="color: green;"></i>
    		   		<input class="ui green basic cancel button" type='submit' id='não' value='Não' style="background-color: white;color: green; font-size: 15px;">
    		</form>
    		<br />
    		<form class='ui  form' action='home' method='post'>
			   <input type='hidden' name='form' value='remover'>
			   <input type='hidden' name='email' value='${user}'>
			   <input type='hidden' name='id' value='${id}'>

    		   		<i class="remove icon" style="color: red;"></i>
    		   		<input class="ui red basic cancel  button" type='submit' value='Sim' style=" background-color: white;color: red; font-size: 15px;">
    		</form>
	   </div>
		
	</div>
	</center>
</body>