<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <!DOCTYPE html>
	<html>
	<head>
		<meta charset='UTF-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
		
		<!-- Site Properties -->
		<link rel='icon' href='Visual/imagens/icon.png' type='image/gif' class='fa fa-bicycle fa-5x circle-icon'> 
		<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
		<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
		<link rel='stylesheet' type='text/css' href='Visual/css/visual.css'>
		<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
		<title>${param.titulo}</title>
		
	</head>
	
	
		