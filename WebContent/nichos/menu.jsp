		<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item' href='${pageContex.request.contexPath}home?pagina=home'>Home</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=ajuda'>Ajuda</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=login'>Login</a>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=cadastro'>Cadastar</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item' href='${pageContex.request.contexPath}home?pagina=home'>Home</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=ajuda'>Ajuda</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=login'>Login</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=cadastro'>Cadastre-se</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='${pageContex.request.contexPath}home?pagina=home'>Home</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=ajuda'>Ajuda</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=login'>Login</a>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=cadastro'>Cadastrar</a>
					</div>
				</div>
			</div>
		</div>