<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- footer -->
	<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
		<div class='ui container'>
			<div class='ui stackable inverted divided equal height stackable grid'>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Links</h4>
					<div class='ui inverted link list'>
						<a href='index.jsp' class='item'>Home</a>
						<a href='login.jspp' class='item'>Login</a>
						<a href='cadastrar.jsp' class='item'>Cadastre-se</a>
					</div>
				</div>
				<div class='three wide column'>
					<h4 class='ui inverted header'>Redes Socias</h4>
					<div class='ui inverted link list'>
						<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
						<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
						<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
					</div>
				</div>
				<div class='seven wide column'>
					<img src='Visual/imagens/icon.png' alt='Imagem da logo' class='ui small image'>
					<h4 class='ui inverted header'>ReportSport</h4>		
				</div>
			</div>
		</div>
	</div>
</body>
</html>