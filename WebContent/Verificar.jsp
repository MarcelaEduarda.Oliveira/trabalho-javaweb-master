<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/core"%>
 <%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("user")==null) {
	request.getRequestDispatcher("Login.jsp").forward(request, response);
}
%>
 <h:import url="nichos/header.jsp"><h:param name="titulo" value="Verificar"></h:param></h:import>
 <!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item'>Seja bem vindo ${sessionScope.user}</a>
				<div class='right menu'>
					<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item'> Seja bem vindo ${param.email}</a>
			<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
		</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item'>Seja bem vindo ${user}</a>
					<div class='right menu'>
						<a class='item' href='${pageContex.request.contexPath}home?pagina=sair'>Sair</a>
					</div>
				</div>
			</div>
		</div>
 		<br><br><br><br>

		<div class='ui middle aligned center aligned grid'>
			<div class='column'>
			<h2 class='ui teal image header'>
      			<img src='Visual/imagens/icon.png' class='image'/>
      				<div class='content'  style='color: #080b34;' >
       					Verificar Funcionário
      				</div>
    		</h2>
    		<div class='ui container'>	
    		<center>	
    		<form class='ui large form' action='home' method='post' style='width: 500px;'>
    		<div class='ui container'>	
				<input type='hidden' name='form' value='veri'>
				<input type='hidden' name='emailtim' value='${user}'>
					<div class='ui stacked segment'>
						<div class='field'>
							<div class='ui left icon input'>
								<i class='user icon'></i>
									<input type='email' name='email' placeholder='Email do funcionario'>
							</div>
						</div>
						<div class='field'>
							<div class='ui left icon input'>
								 <select name='funci'>
      								<option name='funci'>Técnico</option>
      								<option name='funci'>Auxiliar</option>
    							</select>
  							</div>
						</div>
					</div>
					<h:if test='${mensagens.existeErros}'>
            				<div id='erro'>
 
                    				<h:forEach var='erro' items='${mensagens.erros}'>
                        				<h5 style='color: red;'> ${erro} </h5>
                        			</h:forEach>
            				</div>
        			</h:if>
					<input type='submit' class='ui fluid large submit button' style='background-color: #080b34;color: white;' value='Login' />
					</div>
				</form>
				</center>	
			</div>
			</div>
		</div>
	</div>
