package controle;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Erro;


import modelo.Time;
import modelo.Partida;
import modelo.JogadoresModelo;
import modelo.FuncionariosModelo;

@WebServlet("/home")
public class HomeControle<MultipartRequest> extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pagina = request.getParameter("pagina").toString();
		switch(pagina) {
			case "home" :
				request.getRequestDispatcher("home.jsp").forward(request, response);
			break;
			case "ajuda" :
				request.getRequestDispatcher("Ajuda.jsp").forward(request, response);
			break;
			case "cadastro" :
				request.getRequestDispatcher("Cadastro.jsp").forward(request, response);
			break;
			case "login" :
				request.getRequestDispatcher("Login.jsp").forward(request, response);
			break;
			case "verificar":
				request.getRequestDispatcher("Verificar.jsp").forward(request, response);
			break;
			case "relatorio" :
				request.getRequestDispatcher("Relatorio.jsp").forward(request, response);
			break;
			case "atualizarTime" :
				request.getRequestDispatcher("Atualizar.jsp").forward(request, response);
			break;
			case "edite" :
				if(request.getParameter("time") != null) {
					Partida partidaEdit = new ControlePartida().consultarId(Integer.parseInt(request.getParameter("time")));
					JogadoresModelo joga = new JogadoresControle().consultarId(Integer.parseInt(request.getParameter("time")));
					request.setAttribute("partidaEdit", partidaEdit);
					request.setAttribute("jogaEdit", joga);
				}
				request.getRequestDispatcher("Editar.jsp").forward(request, response);
			break;
			case "partida" :
				request.getRequestDispatcher("CadastroPartida.jsp").forward(request, response);
			break;
			case "jogadores" :
				request.getRequestDispatcher("CadastroJogo.jsp").forward(request, response);
			break;
			case "editef" :
				if(request.getParameter("time") != null) {
					FuncionariosModelo funcio = new FuncionariosControle().consultarFuncionarios(request.getParameter("time"));
					request.setAttribute("funcioEdit", funcio);
				}
				request.getRequestDispatcher("Editef.jsp").forward(request, response);
			break;
			case "verFunci":
				if(request.getParameter("time") != null) {
					FuncionariosModelo funcio = new FuncionariosControle().consultarFuncionarios(request.getParameter("time"));
					request.setAttribute("funcioEdit", funcio);
				}
				request.getRequestDispatcher("VerFunci.jsp").forward(request, response);
			break;
			case "rem" :
				if(request.getParameter("time") != null) {
					request.setAttribute("id", request.getParameter("time"));
					request.getRequestDispatcher("Remover.jsp").forward(request, response);
				}
			break;
			case "funcionarios" :
				request.getRequestDispatcher("Funcionario.jsp").forward(request, response);
			break;
			case "principal" :
				if(request.getParameter("time") != null) {
					ArrayList<Partida> lista = new ControlePartida().consultarTime(request.getParameter("time"));
					request.setAttribute("lista", lista);
					request.getRequestDispatcher("Principal.jsp").forward(request, response);
				}
			break;
			case "veri" :
				request.getRequestDispatcher("Verificar.jsp").forward(request, response);
			break;
			case "visualizar" :
				request.getRequestDispatcher("VisualizarAux.jsp").forward(request, response);
			break;
			case "visua" :
				if(request.getParameter("time") != null) {
					ArrayList<Partida> lista = new ControlePartida().consultarTime(request.getParameter("time"));
					request.setAttribute("lista", lista);
					request.setAttribute("lista", lista);
				}
				request.getRequestDispatcher("Relatorio.jsp").forward(request, response);
			break;
			case "sair":
				HttpSession sessao = request.getSession(false);
				if(sessao!=null) {
					sessao.invalidate();
				}
				request.getRequestDispatcher("Login.jsp").forward(request, response);
			break;
			case "jogadoresVisu" :
				if(request.getParameter("time") != null) {
					JogadoresModelo joga = new JogadoresControle().consultarId(Integer.parseInt(request.getParameter("time")));
					request.setAttribute("joga", joga);
					request.getRequestDispatcher("VisuJoga.jsp").forward(request, response);
				}
			break;
			case "jogaVisu" :
				if(request.getParameter("time") != null) {
					JogadoresModelo joga = new JogadoresControle().consultarId(Integer.parseInt(request.getParameter("time")));
					request.setAttribute("joga", joga);
					request.getRequestDispatcher("JogaVisu.jsp").forward(request, response);
				}
			break;
			default :
				request.getRequestDispatcher("404.jsp").forward(request, response);
				
		}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("form").toString();
		Erro erros = new Erro();
		switch(acao) {
			case "cadastro" :
				try{
					Time time = new Time();
					time.setTime(request.getParameter("name"));
					time.setEmail(request.getParameter("email"));
					time.setSenha(request.getParameter("senha"));
					String timee = request.getParameter("email");
					if (new ControleTime().consultarEmail(timee)) {
						erros.add("Email j� existente!");
						erros.add("Verifique se j� est� cadastrado ou troque o email!");
						request.setAttribute("mensagem", erros);
						request.getRequestDispatcher("Cadastro.jsp").forward(request, response);
					}
					if (!erros.isExisteErros()) {
						if(new ControleTime().insertTime(time)) {
							request.getRequestDispatcher("Login.jsp").forward(request, response);
						}
					}
				}catch(IOException e) {
					System.out.println("Erro de PDO: "+e.getMessage());
				}
			break;
			case "login" :
				 ControleTime controle = new ControleTime();
				 String email = request.getParameter("email");
				 String senha = request.getParameter("senha");
				 if(!controle.consultarTime(email, senha)) {
					 erros.add("Email e/ou senha incorreto!");
					 erros.add("Cadastre-se, ou confira seu email/senha!");
					 request.setAttribute("mensagens", erros);
					 request.getRequestDispatcher("Login.jsp").forward(request, response);
				 }
				 if (!erros.isExisteErros()) {
					 HttpSession sessao = request.getSession();
					 sessao.setAttribute("user", email);
					 request.getRequestDispatcher("Funcionario.jsp").forward(request, response);
				 }
			break;
			case "atualizar" :
				 ControleTime com = new ControleTime();
				 Time atuTime = new Time();
				 String em = request.getParameter("email");
				 String sen = request.getParameter("senha");
				 atuTime.setEmail(em);
				 atuTime.setSenha(sen);
				 if(com.atualizarTime(atuTime)) {
					 ArrayList<Partida> list = new ControlePartida().consultarTime(em);
					 request.setAttribute("lista", list);
					request.getRequestDispatcher("Principal.jsp").forward(request, response);
				 }
				 if (erros.isExisteErros()) {
					 erros.add("Email n�o encontrado");
					 request.getRequestDispatcher("Atualizar.jsp").forward(request, response);
				 }
			break;
			case "funcionarios" :
				FuncionariosModelo funcionarios = new FuncionariosModelo();
				funcionarios.setNomet(request.getParameter("nomet"));
				funcionarios.setEmailt(request.getParameter("emailt"));
				funcionarios.setNomea(request.getParameter("nomea"));
				funcionarios.setEmaila(request.getParameter("emaila"));
				String emailt = request.getParameter("emailt");
				String emaila = request.getParameter("emaila");
				funcionarios.setTime(request.getParameter("email"));
				FuncionariosControle control = new FuncionariosControle();
					if(control.consultarEmailt(emailt)) {
						erros.add("Email do T�cnico j� existente!");
						erros.add("Confira se j� est� cadastrado, ou troque o email!");
						request.setAttribute("mensagens", erros);
						request.getRequestDispatcher("Funcionario.jsp").forward(request, response);
					}else{
						if(control.consultarEmaila(emaila)) {
							erros.add("Email do Auxiliar T�cnico j� existente!");
							erros.add("Confira se j� est� cadastrado, ou troque o email!");
							request.setAttribute("mensagens", erros);
							request.getRequestDispatcher("Funcionario.jsp").forward(request, response);
						}else {
							if (!erros.isExisteErros()) {
								if(control.insertFuncionarios(funcionarios)) {
									request.getRequestDispatcher("Verificar.jsp").forward(request, response);
								}
							}
						}
					}

				
			break;
			case "veri":
				String emailf = request.getParameter("email");
				String emailtim = request.getParameter("emailtim");
				FuncionariosControle cont = new FuncionariosControle();
					if(cont.consultarEmailt(emailf)) {
						ArrayList<Partida> lista = new ControlePartida().consultarTime(emailtim);
						request.setAttribute("lista", lista);
						request.getRequestDispatcher("Principal.jsp").forward(request, response);
					}else {
						if(cont.consultarEmaila(emailf)) {
							request.getRequestDispatcher("CadastroPartida.jsp").forward(request, response);
						}else {
							erros.add("Confira se j� est� cadastrado, ou troque o email!");
							request.setAttribute("mensagens", erros);
							request.getRequestDispatcher("Verificar.jsp").forward(request, response);
						}	
					}

				if (erros.isExisteErros()) {
					erros.add("Confira se j� est� cadastrado, ou troque o email!");
					request.setAttribute("mensagens", erros);
					request.getRequestDispatcher("Verificar.jsp").forward(request, response);
				}
			break;
			case "partida" :
					Partida partida = new Partida();
					partida.setLocal(request.getParameter("local"));
					partida.setAdv(request.getParameter("adv"));
					partida.setJuiz(request.getParameter("juiz"));
					partida.setVencedor(request.getParameter("vencedor"));
					partida.setData(request.getParameter("data"));
					partida.setHora(request.getParameter("hora"));
					partida.setTime(request.getParameter("email"));
					if(new ControlePartida().insertPartida(partida)) {
						request.getRequestDispatcher("CadastroJogo.jsp").forward(request, response);
					}else {
						request.getRequestDispatcher("404.jsp").forward(request, response);
					}
			break;
			case "jogadores" :
				JogadoresModelo jogadores = new JogadoresModelo();
				jogadores.setNome1(request.getParameter("nome1"));
				jogadores.setNome2(request.getParameter("nome2"));
				int camisa1 = Integer.parseInt(request.getParameter("camisa1"));
				jogadores.setCamisa1(camisa1);
				jogadores.setCamisa2(Integer.parseInt(request.getParameter("camisa2")));
				jogadores.setPontos1(Integer.parseInt(request.getParameter("pontos1")));
				jogadores.setPontos2(Integer.parseInt(request.getParameter("pontos2")));
				jogadores.setTime(request.getParameter("email"));
				String emailtime = request.getParameter("email");
				if(new JogadoresControle().insertJogadores(jogadores)) {
					ArrayList<Partida> lista = new ControlePartida().consultarTime(emailtime);
					request.setAttribute("lista", lista);
					request.getRequestDispatcher("VisualizarAux.jsp").forward(request, response);
				}else {
					request.getRequestDispatcher("404.jsp").forward(request, response);
				}
			break;
			case "editarpartida" :
				Partida part = new Partida();
				part.setLocal(request.getParameter("local"));
				part.setAdv(request.getParameter("adv"));
				part.setJuiz(request.getParameter("juiz"));
				part.setVencedor(request.getParameter("vencedor"));
				part.setData(request.getParameter("data"));
				part.setHora(request.getParameter("hora"));
				part.setId(Integer.parseInt(request.getParameter("id")));
				String emait = request.getParameter("email");
				if(new ControlePartida().updatePartida(part)) {
					ArrayList<Partida> lista = new ControlePartida().consultarTime(emait);
					ArrayList<JogadoresModelo> list = new JogadoresControle().consultarJogadores(emait);
					request.setAttribute("list", list);
					request.setAttribute("lista", lista);
					request.getRequestDispatcher("Principal.jsp").forward(request, response);
				}else {
					request.getRequestDispatcher("404.jsp").forward(request, response);
				}
			break;
			case "jogadoresEdit" :
				JogadoresModelo joga = new JogadoresModelo();
				joga.setNome1(request.getParameter("nome1"));
				joga.setNome2(request.getParameter("nome2"));
				int camis1 = Integer.parseInt(request.getParameter("camisa1"));
				joga.setCamisa1(camis1);
				joga.setCamisa2(Integer.parseInt(request.getParameter("camisa2")));
				joga.setPontos1(Integer.parseInt(request.getParameter("pontos1")));
				joga.setPontos2(Integer.parseInt(request.getParameter("pontos2")));
				joga.setId(Integer.parseInt(request.getParameter("id")));
				joga.setTime(request.getParameter("email"));
				String timeemail = request.getParameter("email");
				if(new JogadoresControle().updateJogadores(joga)) {
					ArrayList<Partida> lista = new ControlePartida().consultarTime(timeemail);
					request.setAttribute("lista", lista);
					request.getRequestDispatcher("Principal.jsp").forward(request, response);
				}else {
					request.getRequestDispatcher("404.jsp").forward(request, response);
				}
			break;
			case "remover" :
				int id = Integer.parseInt(request.getParameter("id"));
				String emt = request.getParameter("email");
				if(new ControlePartida().deletPartida(id)) {
					if(new JogadoresControle().deletJogadores(id)) {
						ArrayList<Partida> lista = new ControlePartida().consultarTime(emt);
						request.setAttribute("lista", lista);
						request.getRequestDispatcher("Principal.jsp").forward(request, response);
					}
				}
				if (erros.isExisteErros()) {
					erros.add("N�o foi possivel efetuar remo��o");
					request.setAttribute("mensagens", erros);
					request.getRequestDispatcher("Remover.jsp").forward(request, response);
				}	
			break;
			case "voltar" :
				String emti = request.getParameter("email");
				ArrayList<Partida> lista = new ControlePartida().consultarTime(emti);
				request.setAttribute("lista", lista);
				request.getRequestDispatcher("Principal.jsp").forward(request, response);
	
			break;
			case "funcionariosEdit" :
				FuncionariosModelo funcio = new FuncionariosModelo();
				funcio.setNomet(request.getParameter("nomet"));
				funcio.setEmailt(request.getParameter("emailt"));
				funcio.setNomea(request.getParameter("nomea"));
				funcio.setEmaila(request.getParameter("emaila"));
				funcio.setTime(request.getParameter("email"));
				funcio.setId(Integer.parseInt(request.getParameter("id")));
				String emtime = request.getParameter("email");
				if(new FuncionariosControle().updateFunc(funcio)) {
					ArrayList<Partida> list = new ControlePartida().consultarTime(emtime);
					request.setAttribute("lista", list);
					request.getRequestDispatcher("Principal.jsp").forward(request, response);
				}
			default :
				request.getRequestDispatcher("404.JSP").forward(request, response);
		}
	}

}
