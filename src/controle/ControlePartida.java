package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Partida;

public class ControlePartida {
	public boolean insertPartida(Partida partida) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("INSERT INTO partida(time,local,adv,juiz,vencedor,data,hora) VALUES(?,?,?,?,?,?,?);");
			ps.setString(1, partida.getTime() );
			ps.setString(2, partida.getLocal());
			ps.setString(3, partida.getAdv());
			ps.setString(4, partida.getJuiz());
			ps.setString(5, partida.getVencedor());
			ps.setString(6, partida.getData());
			ps.setString(7, partida.getHora());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}
	
	public ArrayList<Partida> consultarTime(String email){
		ArrayList<Partida> lista = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT*FROM partida WHERE time=?;");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
					lista = new ArrayList<Partida>();
					while(rs.next()) {
						Partida partida = new Partida();
						partida.setId(rs.getInt("id"));
						partida.setLocal(rs.getString("local"));
						partida.setAdv(rs.getString("adv"));
						partida.setTime(rs.getString("time"));
						partida.setJuiz(rs.getString("juiz"));
						partida.setVencedor(rs.getString("vencedor"));
						partida.setData(rs.getString("data"));
						partida.setHora(rs.getString("hora"));
						lista.add(partida);
					}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return lista;
	}
	public Partida consultarId(int id){
		Partida partida = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT*FROM partida WHERE id=?;");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
					while(rs.next()) {
						partida = new Partida();
						partida.setId(rs.getInt("id"));
						partida.setLocal(rs.getString("local"));
						partida.setAdv(rs.getString("adv"));
						partida.setTime(rs.getString("time"));
						partida.setJuiz(rs.getString("juiz"));
						partida.setVencedor(rs.getString("vencedor"));
						partida.setData(rs.getString("data"));
						partida.setHora(rs.getString("hora"));
					}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return partida;
	}
	public boolean deletPartida(int Id) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("DELETE FROM partida WHERE id=?");
			ps.setInt(1, Id);
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}

	public boolean updatePartida(Partida par) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("UPDATE partida SET local=?, adv=?, juiz=?, data=?, hora=? WHERE id=?;");
			ps.setString(1, par.getLocal());
			ps.setString(2, par.getAdv());
			ps.setString(3, par.getJuiz());
			ps.setString(4, par.getData());
			ps.setString(5, par.getHora());
			ps.setInt(6, par.getId());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}

	
	
}
