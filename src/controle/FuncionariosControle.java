package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import modelo.FuncionariosModelo;

public class FuncionariosControle {
		public boolean insertFuncionarios(FuncionariosModelo funcionarios) {
			boolean retorno = false;
			try {
				Conexao com = new Conexao();
				PreparedStatement ps = com.getCom().prepareStatement("INSERT INTO funcionarios(nomet,emailt,nomea,emaila,time) VALUES(?,?,?,?,?);");
				ps.setString(1, funcionarios.getNomet());
				ps.setString(2, funcionarios.getEmailt());
				ps.setString(3, funcionarios.getNomea());
				ps.setString(4, funcionarios.getEmaila());
				ps.setString(5, funcionarios.getTime());
				if(!ps.execute()) {
					retorno = true;
				}
				com.FecharConexao();
			}catch(SQLException e) {
				System.out.println("Erro de PDO: "+e.getMessage());
			}catch(Exception e) {
				System.out.println("Erro geral: "+e.getMessage());
			}
			
			return retorno;
		}
		public boolean consultarEmailt(String emailt){
			boolean retorno = false;
			try {
				Conexao com = new Conexao();
				PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM funcionarios WHERE emailt=?;");
				ps.setString(1, emailt);
				ResultSet rs = ps.executeQuery();
				 if(rs.next()){ 
					if(rs.getString("emailt").equals(emailt)) { 
						retorno = true;
					}
				 } else {   
				    retorno = false;
				 }
			}catch(SQLException e) {
				System.out.println("Erro de PDO: "+e.getMessage());
			}catch(Exception e) {
				System.out.println("Erro geral: "+e.getMessage());
			}
			return retorno;
		}
		public boolean consultarEmaila(String emaila){
			boolean retorno = false;
			try {
				Conexao com = new Conexao();
				PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM funcionarios WHERE emaila=?;");
				ps.setString(1, emaila);
				ResultSet rs = ps.executeQuery();
				 if(rs.next()){ 
					if(rs.getString("emaila").equals(emaila)) { 
						retorno = true;
					}
				 } else {   
				    retorno = false;
				 }
			}catch(SQLException e) {
				System.out.println("Erro de PDO: "+e.getMessage());
			}catch(Exception e) {
				System.out.println("Erro geral: "+e.getMessage());
			}
			return retorno;
		}
		
		public FuncionariosModelo consultarFuncionarios(String email){
			FuncionariosModelo funcio = null;
			try {
				Conexao com = new Conexao();
				PreparedStatement ps = com.getCom().prepareStatement("SELECT*FROM funcionarios WHERE time=?;");
				ps.setString(1, email);
				ResultSet rs = ps.executeQuery();
				if(rs != null) {
						funcio = new FuncionariosModelo();
						while(rs.next()) {
							funcio.setEmaila(rs.getString("emaila"));
							funcio.setEmailt(rs.getString("emailt"));
							funcio.setNomea(rs.getString("nomea"));
							funcio.setNomet(rs.getString("nomet"));
							funcio.setId(rs.getInt("id"));
						}
				}
				com.FecharConexao();
			}catch(SQLException e) {
				System.out.println("Erro de SQL: " + e.getMessage());
			}
			return funcio;
		}
		public boolean updateFunc(FuncionariosModelo func) {
			boolean retorno = false;
			try {
				Conexao com = new Conexao();
				PreparedStatement ps = com.getCom().prepareStatement("UPDATE funcionarios SET nomet=?, emailt=?, nomea=?, emaila=? WHERE time=?;");
				ps.setString(1, func.getNomet());
				ps.setString(2, func.getEmailt());
				ps.setString(3, func.getNomea());
				ps.setString(4, func.getEmaila());
				ps.setString(5, func.getTime());
				if(!ps.execute()) {
					retorno = true;
				}
				com.FecharConexao();
			}catch(SQLException e) {
				System.out.println("Erro de SQL:"+e.getMessage());
			}catch(Exception e) {
				System.out.println("Erro geral:"+e.getMessage());
			}
			return retorno;
		}
}
