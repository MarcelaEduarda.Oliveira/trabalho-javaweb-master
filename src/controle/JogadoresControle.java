package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.JogadoresModelo;

public class JogadoresControle {
	public boolean insertJogadores(JogadoresModelo jogadores) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("INSERT INTO jogadores(nome1,nome2,camisa1,camisa2,pontos1,pontos2,time) VALUES(?,?,?,?,?,?,?);");
			ps.setString(1, jogadores.getNome1());
			ps.setString(2, jogadores.getNome2());
			ps.setInt(3, jogadores.getCamisa1());
			ps.setInt(4, jogadores.getCamisa2());
			ps.setInt(5, jogadores.getPontos1());
			ps.setInt(6, jogadores.getPontos2());
			ps.setString(7, jogadores.getTime());
			
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de PDO: "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
		}
		
		return retorno;
	}
	public JogadoresModelo consultarId(int id){
		JogadoresModelo joga = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT*FROM jogadores WHERE id=?;");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
					while(rs.next()) {
						joga = new JogadoresModelo();
						joga.setId(rs.getInt("id"));
						joga.setNome1(rs.getString("nome1"));
						joga.setNome2(rs.getString("nome2"));
						joga.setCamisa1(rs.getInt("camisa1"));
						joga.setCamisa2(rs.getInt("camisa2"));
						joga.setPontos1(rs.getInt("pontos1"));
						joga.setPontos2(rs.getInt("pontos2"));
					}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return joga;
	}
	public boolean updateJogadores(JogadoresModelo joga) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("UPDATE jogadores SET nome1=?, nome2=?, camisa1=?, camisa2=?, pontos1=?, pontos2=? WHERE id=?;");
			ps.setString(1, joga.getNome1());
			ps.setString(2, joga.getNome2());
			ps.setInt(3, joga.getCamisa1());
			ps.setInt(4, joga.getCamisa2());
			ps.setInt(5, joga.getPontos1());
			ps.setInt(6, joga.getPontos2());
			ps.setInt(7, joga.getId());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}
	public boolean deletJogadores(int Id) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("DELETE FROM jogadores WHERE id=?");
			ps.setInt(1, Id);
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}
	public ArrayList<JogadoresModelo> consultarJogadores(String email){
		ArrayList<JogadoresModelo> lista = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT*FROM jogadores WHERE time=?;");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
					lista = new ArrayList<JogadoresModelo>();
					while(rs.next()) {
						JogadoresModelo jogada = new JogadoresModelo();
						jogada.setNome1(rs.getString("nome1"));
						jogada.setNome2(rs.getString("nome2"));
						jogada.setCamisa1(rs.getInt("camisa1"));
						jogada.setCamisa2(rs.getInt("camisa2"));
						jogada.setPontos1(rs.getInt("pontos1"));
						jogada.setPontos2(rs.getInt("pontos2"));
						lista.add(jogada);
					}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return lista;
	}
}

