package controle;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;

import modelo.Time;

public class ControleTime {
	public boolean insertTime(Time time) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("INSERT INTO time(nome,email,senha) VALUES(?,?,?);");
			ps.setString(1, time.getTime());
			ps.setString(2, time.getEmail());
			ps.setString(3, time.getSenha());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de PDO: "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
		}
		
		return retorno;
	}
	public boolean consultarTime(String email, String senha){
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM time WHERE email=? AND senha=?;");
			ps.setString(1, email);
			ps.setString(2, senha);
			ResultSet rs = ps.executeQuery();
			 if(rs.next()){ 
				if(rs.getString("email").equals(email) && rs.getString("senha").equals(senha)) { 
					retorno = true;
				}
			 } else {   
			    retorno = false;
			 }
		}catch(SQLException e) {
			System.out.println("Erro de PDO: "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
		}
		return retorno;
	}
	public boolean consultarEmail(String email){
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM time WHERE email=?;");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			 if(rs.next()){ 
				if(rs.getString("email").equals(email)) { 
					retorno = true;
				}
			 } else {   
			    retorno = false;
			 }
		}catch(SQLException e) {
			System.out.println("Erro de PDO: "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
		}
		return retorno;
	}
	public ArrayList<Time> selecionarTodos(){
		ArrayList<Time> lista = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM time;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Time>();
				while(rs.next()) {
					Time time = new Time();
					time.setId_t(rs.getInt("id"));
					time.setTime(rs.getString("nome"));
					time.setEmail(rs.getString("email"));
					time.setSenha(rs.getString("senha"));
					lista.add(time);
				}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return lista;
	}
	public boolean atualizarTime(Time time) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("UPDATE time SET senha=?  WHERE email=?;");
			ps.setString(1, time.getSenha());
			ps.setString(2, time.getEmail());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}
}
