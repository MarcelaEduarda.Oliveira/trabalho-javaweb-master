package modelo;

public class FuncionariosModelo {
	private int id;
	private String nomet;
	private String nomea;
	private String emailt;
	private String emaila;
	private String time;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomet() {
		return nomet;
	}
	public void setNomet(String nomet) {
		this.nomet = nomet;
	}
	public String getNomea() {
		return nomea;
	}
	public void setNomea(String nomea) {
		this.nomea = nomea;
	}
	public String getEmailt() {
		return emailt;
	}
	public void setEmailt(String emailt) {
		this.emailt = emailt;
	}
	public String getEmaila() {
		return emaila;
	}
	public void setEmaila(String emaila) {
		this.emaila = emaila;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
