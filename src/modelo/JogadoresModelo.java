package modelo;

public class JogadoresModelo {
	private int id;
	private String nome1;
	private String nome2;
	private int camisa1;
	private int camisa2;
	private String time;
	private int pontos1;
	private int pontos2;
	public int getPontos1() {
		return pontos1;
	}
	public void setPontos1(int pontos1) {
		this.pontos1 = pontos1;
	}
	public int getPontos2() {
		return pontos2;
	}
	public void setPontos2(int pontos2) {
		this.pontos2 = pontos2;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome1() {
		return nome1;
	}
	public void setNome1(String nome1) {
		this.nome1 = nome1;
	}
	public String getNome2() {
		return nome2;
	}
	public void setNome2(String nome2) {
		this.nome2 = nome2;
	}
	public int getCamisa1() {
		return camisa1;
	}
	public void setCamisa1(int camisa1) {
		this.camisa1 = camisa1;
	}
	public int getCamisa2() {
		return camisa2;
	}
	public void setCamisa2(int camisa2) {
		this.camisa2 = camisa2;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
	
}