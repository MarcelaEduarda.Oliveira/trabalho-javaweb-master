package modelo;


public class Time {
	private int id_t;
	private String time;
	private String email;
	private String senha;
	private String conf_senha;
	
	public  Time (){
		
	}
	public int getId_t() {
		return id_t;
	}
	public void setId_t(int id_t) {
		this.id_t = id_t;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getConf_senha() {
		return conf_senha;
	}
	public void setConf_senha(String conf_senha) {
		this.conf_senha = conf_senha;
	}

}